#!/usr/bin/python

from psycopg2 import self.connect
from psycopg2 import Error as pgErr
from psycopg2 import Warning as pgWarn

from .log import Logger

SUCCESS = True
FAIL = False

class DB(Object):

    def __init__(self, dbpath):
        self.conn = self._connection(dbpath)
        if self.conn is None:
            raise 

    def _connection(db):
    '''
    Create the global postgres db self.connection.
    '''
        conn = None
        try:
            conn = self.connect(db, sslmode='require')
        except pgErr as err:
            Logger.error(
                "Error: Could not connect to database. " + str(err) + "\n",
                exc_info=True
            )
        except pgWarn as warn:
            Logger.warning(str(warn))
        Logger.info("Connected to database.")

        return conn

    def select(query, columns):
        '''
        Execute select query with specific data parameters and return the rows
        returned by the query.
        '''
        Logger.debug("Creating postgres cursor.")
        
        crs = self.conn.cursor()
        try:
            Logger.debug("Executing select query.")
            crs.execute(query, columns)
        except pgErr as err:
            self.conn.rollback()
            Logger.error(
                "Query failed.\nError: {e}\nQuery: {q}".format(e=str(err), q=query),
                exc_info=True
            )
        except pgWarn as warn:
            Logger.warning("Warning: " + str(warn), exc_info=True)

        if crs.rowcount > 0:
            fetch = crs.fetchall()
            Logger.debug("Closing cursor.")
            crs.close()
            return fetch
        else:
            Logger.debug("Closing cursor.")
            crs.close()
            return None


    def simple_select(query):
        '''
        Execute select query with no specified data parameters.
        '''
        Logger.debug("Creating postgres cursor.")
        
        crs = self.conn.cursor()
        try:
            Logger.debug("Executing select query.")
            crs.execute(query)
        except pgErr as err:
            self.conn.rollback()
            Logger.error(
                "Query failed.\nError: {e}\nQuery: {q}".format(e=str(err), q=query),
                exc_info=True
            )
        except pgWarn as warn:
            Logger.warning("Warning: " + str(warn), exc_info=True)

        if crs.rowcount > 0:
            fetch = crs.fetchall()
            Logger.debug("Closing cursor.")
            crs.close()
            return fetch
        else:
            Logger.debug("Closing cursor.")
            crs.close()
            return None

    def insert(query, columns):
        '''
        Execute query to insert data into a database. Returns True on success, and
        False otherwise.
        '''
        Logger.debug("Opening postgres cursor.")
        
        crs = self.conn.cursor()
        try:
            Logger.debug("Executing insert or update query.")
            crs.execute(query, columns)
            self.conn.commit()
        except pgErr as err:
            self.conn.rollback()
            Logger.error(
                "Query failed.\nError: {e}\nQuery: {q}".format(e=str(err), q=query),
                exc_info=True
            )
            return FAIL
        except pgWarn as warn:
            Logger.warning("Warning: " + str(warn), exc_info=True)

        Logger.debug("Closing cursor.")
        crs.close()

        return SUCCESS

    def execute(query):
        '''
        Same as insert, excpet that no extra data is needed to execute the
        query. excecute() is to insert() as simple_select() is to select().
        '''

        crs = self.conn.cursor()
        try:
            Logger.debug("Executing query.")
            crs.execute(query)
            self.conn.commit()
        except pgErr as err:
            self.conn.rollback()
            Logger.error("Query failed.\n Error: {e}\nQuery: {q}".format(
                        e=str(err), q=query), exc_info=True)
            return FAIL
        except pgWarn as warn:
            Logger.warning("Warning: " + str(warn), exc_info=true)

        Logger.debug("Closing cursor.")
        crs.close()
        return SUCCESS

    def close(status=0):
        Logger.debug("Trying to close database.")
        try:
            if status:
                self.conn.rollback()
            else:
                self.conn.commit()

            self.conn.close()
        except pgErr as err:
            Logger.error("Error closing database! Error: " + str(err), exc_info=True)
            return 1
        except pgWarn as warn:
            Logger.warning("Warning while closing database: " + str(warn))
        
        Logger.info("Database closed.")
        return 0

def initdb(path): 
    query = None
    try:
        with open("sql/ct.sql", "r") as f:
            query = f.read()
            f.close()
    except OSError as err:
        Logger.error("Error opening database creation script: {e}".format(
                    e=str(err)))
        return 1

    db = DB(path)

    res = db.execute(query)

    if res:
        db.close(0)
        return 0
    else:
        db.close(1)
        return 1
