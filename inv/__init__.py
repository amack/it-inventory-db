#!/usr/bin/python

from logging import basicConfig
from argparse import ArgumentParser
from sys import exit

import .inv
from .db import initdb
from .log import Logger

BADPARSE = "No valid operation arguments found, but the parser raised no errors."

def main():
    basicConfig(
        format="[%(asctime)s][%(levelname)s]: %(message)s",
        level=environ['LOG_LVL'],
        stream=stdout
    )
    
    parser = ArgumentParser()
    
    #args for which operation is performed. op short for operation
    op = parser.add_mutually_exclusive_group(require=True)
    op.add_argument('--init', action='store_true', dest='init')
    op.add_argument('--insert', action='store_true', dest='insert')
    op.add_argument('--export', action='store_true', dest='export')
    op.add_argument('--query', action='store_true', dest='query')

    parser.add_argument('-f', '--file', dest='csvf', type=str, required=True)

    parsed = parser.parse_args()

    csvf = parsed['csvf']

    if parsed['init']:
        return initdb("path to db")

    elif parsed['insert']:
        return inv.insert(csvf)

    elif parsed['export']:
        return inv.export(csvf)

    elif parsed['query']:
        pass

    else:
        raise RuntimeError(BADPARSE)
        return 1

    return 0

if __name__ == "__main__":
    res = main()
    exit(res)
