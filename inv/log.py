#!/usr/bin/python

import logging

def logger(level: str):
    log = logging.getLogger()
    log.setLevel(level)

    return log

Logger = logger()
