#!/usr/bin/python

from csv import DictReader

from .db import DB
from.log import Logger

def init(): #not to be confused with __init__ :)


dbpath = '' #put database path here

INST = """
    INSERT INTO inst VALUES ((SELECT 
                max(SELECT inst_num FROM inst WHERE inst_item_serl = %s)),
                %s, %s);"""

def insert(csvf):
    data = []

    try:
        with open(csvf) as f:
            reader = DictReader(f)
            for row in reader:
                data.append(row)

    except OSError as err:
        Logger.error(str(err), exc_info=True)
        return 1

    d = DB(dbpath)
    try:
        emps = []
        rows1 = d.simple_select("SELECT (emp_mail) FROM emp;")
        if rows1 is not None:
            for row in rows1:
                emps.append(row[0])

        serl = []
        rows2 = d.simple_select("SELECT (item_serl) FROM item;")
        if rows2 is not None:
            for row in rows2:
                modls.append(row[0])

        for item in data:
            if item['item_ownr'] not in emps:
                res = d.insert("INSERT INTO emp VALUES (%s);",
                        (item['item_owner'],))
                if not res:
                    return 1

            if item['serial_number'] not in serls:
                res = d.insert("INSERT INTO item VALUES (%s, %s, %s, %s, %s);",
                        (item['serial_number'], item['item_name'],
                            item['item_vend'], item['item_ctgy'],
                            item['item_desc']))
                if not res:
                    return 1

            #'inst_item_serl' is supposed to be here twice
            res = d.insert(INST, (item['inst_item_serl'],
                        item['inst_item_serl'], item['inst_emp_mail']))
            if not res:
                return 1

    finally:
        d.close()
    
    return 0

EXPT = """
    SELECT * FROM item INNER JOIN inst ON 
    (item.item_serl == inst.inst_item_serl) 
    INNER JOIN emp ON (emp.emp_mail == inst.inst_emp_mail);"""

def export(csvf):
    
    d = DB(dbpath)

    rows = d.simple_select(EXPT)
    
    datadicts = []
    for row in rows:
        data = {
            "Serial Number":,
            "
        
