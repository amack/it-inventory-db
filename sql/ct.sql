/*
    CT_BENZINGA_ITDB.sql
    Andrew Mack
    2018-09-27

    Create tables for IT inventory database
*/

DO $$ BEGIN
    CREATE TYPE ctgy AS ENUM (
            'video'
    );
EXCEPTION
    WHEN duplicate_object THEN null;
END $$;

--item
CREATE TABLE item(
    item_serl text PRIMARY KEY,
    item_desc text NOT NULL,
    item_vend text NOT NULL,
    item_ctgy ctgy NOT NULL,
    item_desc2 text
    );

--employee
CREATE TABLE emp(
    emp_mail text PRIMARY KEY,
    emp_crnt boolean DEFAULT 'yes'
    );

--item instance
CREATE TABLE inst(
    inst_num integer,
    inst_item_serl text REFERENCES item(item_serl),
    inst_emp_mail text REFERENCES emp(emp_mail),
    inst_rec timestamptz DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(inst_num, inst_item_modl)
    );
